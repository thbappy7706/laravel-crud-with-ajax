# Laravel Crud with ajax

CRUD stands for Create, Read, Update and Delete which are operations needed in most data-driven apps that access and work with data from a database. In this example, we'll see how to impelement the CRUD operations in Laravel  against a MySQL database